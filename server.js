/* jshint node: true, laxcomma: true */
"use strict";

var express = require("express")
  , lessMiddleware = require("less-middleware")
  , path = require("path")
  , fs = require("fs")
  , uuid = require("node-uuid")
  , app = express()
  , PORT = 3000
  , DEST_PATH = path.join(__dirname, ".bin")
  , mkdirp = require("mkdirp")
  , child_process = require("child_process")
  ;

app.use(lessMiddleware({
    src: path.join(__dirname, "private")
  , dest: DEST_PATH
}));
app.use(express.bodyParser());
app.use(express.static( path.join(__dirname, "public") ));
app.use(express.static( DEST_PATH ));
app.use(express.static( path.join(__dirname, "uploads") ));

app.post("/upload", function (req, res) {

  var tmpPath = path.join("uploads", uuid.v4())
    , fullTmpPath = path.join("uploads", tmpPath)
    , videoReadStream
    , audioReadStream
    , videoWriteStream
    , audioWriteStream
    , videoFilename = path.join(fullTmpPath, "video.webm")
    , audioFilename = path.join(fullTmpPath, "audio.wav")
    , outputName = path.join(fullTmpPath, "output.webm")
    , copied = (function () {
      var count = 0;
      return function () {
        count++;
        if (count >= 2) {
          finishedCopying();
        }
      };
    })()
    , finishedCopying = function () {
      var ffmpeg = child_process.spawn("ffmpeg", [
          "-i"
        , videoFilename
        , "-i"
        , audioFilename
        , "-c:v"
        , "copy"
        , "-c:a"
        , "libvorbis"
        , "-q:a"
        , "24"
        , outputName]);

        ffmpeg.on("close", function () {
          console.log("Converted.");
          res.json({message: "Success!", url: "/" + tmpPath + "/output.webm"});
        });
      }
    ;
  mkdirp.sync(fullTmpPath);
  videoReadStream =
    fs.createReadStream(req.files.video.path);
  audioReadStream =
    fs.createReadStream(req.files.audio.path);
  videoWriteStream =
    fs.createWriteStream(videoFilename);
  audioWriteStream =
    fs.createWriteStream(audioFilename);

  videoWriteStream.on("finish", function () {
    copied();
  });

  audioWriteStream.on("finish", function () {
    copied();
  });

  videoReadStream.pipe(videoWriteStream);
  audioReadStream.pipe(audioWriteStream);
});

app.listen(PORT, "0.0.0.0");
console.log("Server listening on port " + PORT + ".");