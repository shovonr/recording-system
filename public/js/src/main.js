/* globals requirejs, define  */

requirejs.config({
    baseUrl: ""
  , paths: {
      jquery    : "js/lib/jquery-2.0.3.min"
    , underscore: "js/lib/lodash.min"
    , backbone  : "js/lib/backbone-min"
    , bootstrap : "bootstrap/js/boottrap"
    , async     : "js/lib/async"
  }
  , shim: {
      bootstrap: {
        deps: ["jquery"]
      , exports: "$.fn.popover"
    }
  }
  , enforceDefine: true
});

define([
    "require"
  , "jquery"
  , "js/src/views/WaitingView"
  , "js/src/views/PreviewView"
  , "js/src/views/RecordingView"
  , "js/src/views/CompilingView"
], function (require) {
  "use strict";

  var $ = require("jquery")
    , WaitingView = require("js/src/views/WaitingView")
    , PreviewView = require("js/src/views/PreviewView")
    , RecordingView = require("js/src/views/RecordingView")
    , CompilingView = require("js/src/views/CompilingView")
    ;

  $(function () {
    var waitingView = new WaitingView({
      el: document.getElementById("waiting-view")
    });

    waitingView.getUserMedia(function (err, results) {
      var previewView = new PreviewView({
            el: document.getElementById("preview-area")
          , videoStream: results.videoStream
          , audioStream: results.audioStream
        })
        , recordingView = new RecordingView({
            el: document.getElementById("recording-view")
          , videoElement: previewView.getVideoElement()
          , audioStream: results.audioStream
        })
        ;
      waitingView.$el.hide();
      recordingView.on("stop-recording", function () {
        var compilingView = new CompilingView({
              el: document.getElementById("compiling-view")
            , frames: recordingView.getFrames()
            , elapsedTime: recordingView.getElapsedTime()
            , audioRecorder: recordingView.getAudioRecorder()
          })
          ;
        recordingView.$el.hide();
        compilingView.$el.show();
        compilingView.on("rendered", function (url) {
          var linkView = new LinkView({
            url: url
          });
          linkView.$el.show();
        });
        compilingView.render();
        //window.alert("You have successfully finished recording this shit.");
      });
      recordingView.$el.show();
      previewView.render();
    });
  });
});
