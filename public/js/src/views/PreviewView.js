/* globals define */

define([
    "require"
  , "backbone"
  , "js/lib/AudioOutput"
  , "js/lib/CanvasContext2d"
], function (require) {
  "use strict";

  var Backbone = require("backbone")
    , AudioOutput = require("js/lib/AudioOutput")
    , CanvasContext2d = require("js/lib/CanvasContext2d")
    ;

  return Backbone.View.extend({
      _audioOutput: null
    , _videoTag: null

    , initialize: function () {
      this._audioOutput = new AudioOutput(this.options.audioStream);
      this._videoTag = this.$el.find(".webcam-output")[0];
    }

    , _renderAudio: function () {
      var canvas = this.$el.find(".analyser")[0]
        , canvasContext2d
        , self = this;
      canvasContext2d = new CanvasContext2d(canvas);
      canvasContext2d.setClearColor("black");
      canvasContext2d.onDraw(function () {
        var width = canvasContext2d.getWidth()
          , height = canvasContext2d.getHeight()
          , magnitude = 2 * self._audioOutput.getDecibel()
          ;

        canvasContext2d.fillRect(
          0
        , height - magnitude
        , width
        , magnitude
        , "green"
        );
      });
    }

    , getVideoElement: function () {
      return this._videoTag;
    }

    , render: function () {
      this._videoTag.autoplay = true;
      this._videoTag.src = window.URL.createObjectURL(this.options.videoStream);

      this._renderAudio();
    }
  });
});