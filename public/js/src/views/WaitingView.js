/* globals define */

define(["require", "backbone", "async", "js/lib/utils"], function (require) {
  "use strict";

  var Backbone = require("backbone")
    , _ = require("js/lib/utils")
    , async = require("async");

  return Backbone.View.extend({
      initialize: function () {}

    , _gotWebcam: function () {
      this.$(".webcam-waiting").remove();
    }
    , _gotMic: function () {
      this.$(".mic-waiting").remove();
    }

    , getUserMedia: function (callback) {
      var self = this
        , getUserMediaAsync = function (option, method, callback) {
          var obj = {}; obj[option] = true;
          _.getUserMediaAsync(obj, function (err, stream) {
            if (err) { return callback(err); }
            self[method]();
            callback(null, stream);
          });
        }
        ;

      async.parallel({
          videoStream: async.apply(getUserMediaAsync, "video", "_gotWebcam")
        , audioStream: async.apply(getUserMediaAsync, "audio", "_gotMic")
      }, callback);
    }
  });
});
