/* globals define */

define(["require", "backbone", "js/lib/VideoRecorder"], function (require) {
  "use strict";

  var Backbone = require("backbone")
    , VideoRecorder = require("js/lib/VideoRecorder")

    , VIDEO_WIDTH = 320
    , VIDEO_HEIGHT = 240
    , MAX_RECORDING_TIME = 10 * 1000
    ;

  var RecordingView = Backbone.View.extend({
      _videoElement: null
    , _videoRecorder: null
    , _progressFrameId: null
    , _recording: false
    , _timeStarted: null
    , _recordingTime: null
    , _audioContext: null
    , _audioRecorder: null
    , _audioStream: null

    , initialize: function () {
      var recordButton = this.$(".record-button")
        , self = this;

      this._videoElement = this.options.videoElement;
      this._audioContext = new window.AudioContext();
      this._audioStream = this.options.audioStream;
      console.log(this.options.audioStream);

      this._videoRecorder = new VideoRecorder(
        this._videoElement
      , VIDEO_WIDTH
      , VIDEO_HEIGHT
      );

      recordButton.mousedown(function () {
        self._startRecording();
      });

      recordButton.mouseup(function () {
        self._stopRecording();
      });
    }

    , getFrames: function () {
      return this._videoRecorder.getFrames();
    }

    , getAudioRecorder: function () {
      return this._audioRecorder;
    }

    , getElapsedTime: function () {
      return this._videoRecorder.getElapsedTime();
    }

    , _animateProgressBar: function () {
      var self = this
        , $progressBar = this.$el.find(".progress .bar")
        , $progressText = this.$el.find(".progress .progress-text")
        , animate = function () {
          var timeElapsed = (new Date()).getTime() - self._timeStarted
            , remainingTime = (MAX_RECORDING_TIME - timeElapsed) / 1000 | 0
            , percentage = timeElapsed / MAX_RECORDING_TIME;
          if (percentage > 1) { percentage = 1; }
          percentage = (percentage * 100 | 0);
          self._progressFrameId = window.requestAnimationFrame(animate);
          $progressBar.css("width", percentage + "%");
          $progressText.html(
            remainingTime < 10 ? "0:0" + remainingTime : "0:" + remainingTime
          );
        }
        ;
      this._timeStarted = new Date().getTime();
      animate();
    }

    , _stopProgressBarAnimation: function () {
      window.cancelAnimationFrame(this._progressFrameId);
      this._progressFrameId = null;
    }

    , _startRecording: function () {
      var self = this;

      if (this._recording) { return; }
      console.log(window.Recorder);
      this._audioRecorder = new window.Recorder(
        this._audioContext.createMediaStreamSource(this._audioStream));
      this._audioRecorder.record();
      this._videoRecorder.startRecording();

      setTimeout(function () {
        self._stopRecording();
      }, MAX_RECORDING_TIME);

      this._animateProgressBar();
      this.$el.find(".record-button").html(RecordingView.STOP_RECORDING_MESSAGE);

      this._recording = true;
    }

    , _stopRecording: function () {
      if (!this._recording) { return; }

      this._audioRecorder.stop();
      this._videoRecorder.stopRecording();
      this._stopProgressBarAnimation();

      this._recording = false;
      this.trigger("stop-recording");
    }
  });

  RecordingView.STOP_RECORDING_MESSAGE = "Release to stop recording.";

  return RecordingView;
});
