/* globals define */

define(["require", "backbone"], function (require) {
  "use strict";

  var Backbone = require("backbone")
    ;

  return Backbone.View.extend({
      _webmBlob: null
    , _videoUrl: null

    , initialize: function () {
      if (!this.options.elapsedTime) { throw new Error("An elapsed time should have been specified."); }
      if (!this.options.frames) { throw new Error("Expected an array of frames."); }
      if (!this.options.audioRecorder) { throw new Error("Expected a audio recorder."); }
    }
    , render: function () {
      var frames = this.options.frames
        , elapsedTime = this.options.elapsedTime
        , xhr = new XMLHttpRequest()
        , self = this
        ;
      
      this._webmBlob = window.Whammy.fromImageArray(frames, 1000 / 50);

      this.options.audioRecorder.exportWAV(function (blob) {
        var form = new FormData()
          , xhr;

        form.append("video", self._webmBlob);
        form.append("audio", blob);

        xhr = new XMLHttpRequest();
        xhr.readStateChanged = function () {
          self.trigger("rendered");
        };
        xhr.open("POST", "/upload");
        xhr.send(form);
      });
    }
  });
});