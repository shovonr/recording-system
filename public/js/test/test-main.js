/* globals requirejs */
(function () {
  "use strict";

  var tests = Object.keys(window.__karma__.files).filter(function (file) {
    return (/Spec\.js$/).test(file);
  });

  requirejs.config({
    // Karma serves files from '/base'
    baseUrl: "/base",

    paths: {
      "jquery": "js/lib/jquery-2.0.3.min",
      "underscore": "js/lib/lodash.min",
      "backbone": "js/lib/backbone-min",
      "async": "js/lib/async",
      "sinon": "js/test/sinon-1.7.3"
    },

    shim: {},

    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
  });
})();