/* globals describe, beforeEach, sinon, afterEach, it, AsyncSpec, define, expect */

define([
    "require"
  , "js/src/views/WaitingView"
  , "js/lib/utils"
  , "jquery"
]
  , function (require) {
  "use strict";

  var WaitingView = require("js/src/views/WaitingView")
    , $ = require("jquery")
    //, _ = require("js/lib/utils")
    ;

  describe("WaitingView", function () {

    describe("#_gotWebcam", function () {
      var waitingAreaDOM;

      beforeEach(function () {
        waitingAreaDOM = document.createElement("div");
        var childDOM = document.createElement("div");
        childDOM.setAttribute("class", "webcam-waiting");
        waitingAreaDOM.appendChild(childDOM);
      });

      afterEach(function () {
        $(waitingAreaDOM).remove();
      });

      it("should be deleting the webcam waiting message", function () {
        var waitingView = new WaitingView({
            el: waitingAreaDOM
          })
          , waitingMessage
          ;

        waitingView._gotWebcam();

        waitingMessage = waitingAreaDOM.querySelector("*");
        expect(waitingMessage).toBe(null);
      });
    });

    describe("#_gotMic", function () {
      var waitingAreaDOM;

      beforeEach(function () {
        waitingAreaDOM = document.createElement("div");
        var childDOM = document.createElement("div");
        childDOM.setAttribute("class", "mic-waiting");
        waitingAreaDOM.appendChild(childDOM);
      });

      afterEach(function () {
        $(waitingAreaDOM).remove();
      });

      it("should be deleting the mic waiting message", function () {
        var waitingView = new WaitingView({
            el: waitingAreaDOM
          })
          , waitingMessage
          ;

        waitingView._gotMic();

        waitingMessage = waitingAreaDOM.querySelector("*");
        expect(waitingMessage).toBe(null);
      });
    });

    describe("#getUserMedia", function () {
      var async = new AsyncSpec(this)
        , _getUserMedia
        ;

      async.beforeEach(function (done) {
        _getUserMedia = navigator.getUserMedia;
        navigator.getUserMedia = function (options, callback) {
          callback({});
        };
        done();
      });

      async.afterEach(function (done) {
        navigator.getUserMedia = _getUserMedia;
        done();
      });

      async.it("should be able to get both audio and video stream, as well as delete both messages.", function (done) {
        var waitingView = new WaitingView();

        sinon.spy(waitingView, "_gotWebcam");
        sinon.spy(waitingView, "_gotMic");

        waitingView.getUserMedia(function (err) {
          if (err) { throw err; }
          expect(waitingView._gotWebcam.called).toBe(true);
          expect(waitingView._gotMic.called).toBe(true);
          done();
        });
      });
    });
  });
});