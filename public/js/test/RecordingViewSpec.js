/* globals define, describe, it, beforeEach, afterEach, expect */

define(["require", "jquery", "js/src/views/RecordingView"], function (require) {
  "use strict";

  var RecordingView = require("js/src/views/RecordingView")
    , $ = require("jquery")
    ;

  describe("RecordingView", function () {
    describe("#_startRecording and #_stopRecording", function () {
      var _requestAnimationFrame
        , _cancelAnimationFrame
        , _canvasContext2DDrawImage
        , recordingDOM
        ;
      
      beforeEach(function () {
        // We don't need to do any actual animation.
        _requestAnimationFrame = window.requestAnimationFrame;
        _cancelAnimationFrame = window.cancelAnimationFrame;
        
        window.requestAnimationFrame = function (fn) {
          return setInterval(fn, 1000 / 60);
        };

        window.cancelAnimationFrame = function (id) {
          clearInterval(id);
        };

        recordingDOM = document.createElement("div");
        recordingDOM.innerHTML =
          "<div class=\"progress with-text\">" +
            "<div class=\"bar\" style=\"width: 0;\"</div>" +
            "<span class=\"progress-text\"></span>" +
          "</div>" +
          "<button class=\"btn btn-primary record-button\" type=\"\">" +
            "<span class=\"record-icon\">&#9679;</span> Click and Hold to Record" +
          "</button>";

        // For some odd reason, PhantomJS does not like displaying video
        // on a canvas element. Silence the error message by simply overriding
        // the `CanvasRenderingContext2D#drawImage` method.
        _canvasContext2DDrawImage =
          window.CanvasRenderingContext2D.prototype.drawImage;
        window.CanvasRenderingContext2D.prototype.drawImage = function () {};
      });

      afterEach(function () {
        window.requestAnimationFrame = _requestAnimationFrame;
        window.CanvasRenderingContext2D = _canvasContext2DDrawImage;
      });

      it(
        "should have its recording flag set to true, the recording button's " +
        "text changed, and the progress bar updating. Upon stop, the flag " +
        "will be set to false and the data ", function () {
        var recordingView = new RecordingView({
              el: recordingDOM
            , videoElement: document.createElement("video")
          })
          , recordButtonText
          , stopRecordingListener = window.sinon.spy()
          ;
        recordingView.on("stop-recording", stopRecordingListener);
        window.sinon.spy(recordingView._videoRecorder, "startRecording");
        recordingView._startRecording();
        expect(recordingView._recording).toBe(true);
        recordButtonText = $(recordingDOM).find(".record-button").html();
        expect(recordButtonText).toBe(RecordingView.STOP_RECORDING_MESSAGE);
        expect(recordingView._progressFrameId).not.toBe(null);
        expect(recordingView._timeStarted).not.toBe(null);

        recordingView._startRecording();
        expect(recordingView._videoRecorder.startRecording.calledOnce).toBe(true);

        recordingView._stopRecording();
        expect(stopRecordingListener.calledOnce).toBe(true);
        expect(recordingView._recording).toBe(false);
        expect(recordingView._progressFrameId).toBe(null);
      });
    });
  });
});
