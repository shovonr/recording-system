/* globals define, describe, AsyncSpec */

define(["require", "js/lib/utils"], function (require) {
  "use strict";

  var utils = require("js/lib/utils");

  describe("utils", function () {
    describe(".getUserMediaAsync", function () {
      var _getUserMedia
        , async = new AsyncSpec(this);

      async.beforeEach(function (done) {
        _getUserMedia = navigator.getUserMedia;
        navigator.getUserMedia = function (options, callback) {
          utils.defer(function () {
            callback({});
          });
        };
        done();
      });

      async.afterEach(function (done) {
        navigator.getUserMedia = _getUserMedia;
        done();
      });

      async.it("should always return an object with audioStream and videoStream properties", function (done) {
        utils.getUserMediaAsync({video: true}, function (err) {
          if (err) { throw err; }
          // TODO: test this better.
          done();
        });
      });
    });
  });
});