/* globals define */

define([], function () {
  "use strict";

  function VideoRecorder(videoElement, width, height) {
    var frameId
      , frames = []
      , timeStarted
      , timeEnded
      ;

    this.startRecording = function () {
      var tempCanvas = document.createElement("canvas")
        , context = tempCanvas.getContext("2d")
        , drawVideoFrame = function () {
          frameId = window.requestAnimationFrame(drawVideoFrame);
          context.drawImage(videoElement, 0, 0, width, height);
          frames.push(tempCanvas.toDataURL("image/webp", 1));
        }
        ;

      tempCanvas.width = width;
      tempCanvas.height = height;

      timeStarted = new Date().getTime();
      drawVideoFrame();
    };

    this.stopRecording = function () {
      timeEnded = new Date().getTime();
      window.cancelAnimationFrame(frameId);
    };

    this.getFrames = function () {
      return frames;
    };

    this.getElapsedTime = function () {
      return timeEnded - timeStarted;
    };
  }

  return VideoRecorder;
});
