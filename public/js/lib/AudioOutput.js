/* globals define */

define([], function () {
  "use strict";

  var audioContext = new window.AudioContext();

  function AudioOutput(stream) {
    var analyser = audioContext.createAnalyser()
      , microphone = audioContext.createMediaStreamSource(stream)
      ;

    microphone.connect(analyser);

    this.getFrequencyData = function () {
      var fftdata = new Uint8Array(analyser.frequencyBinCount);
      analyser.getByteFrequencyData(fftdata);
      return fftdata;
    };

    this.getDecibel = function () {
      var freq = Array.prototype.slice.call(this.getFrequencyData())
        , sum
        ;

      freq = freq.slice((freq.length / 20) | 0, (freq.length / 10) | 0);
      sum = freq.reduce(function (prev, curr) { return prev + curr; });

      return sum / freq.length;
    };
  }

  return AudioOutput;
});