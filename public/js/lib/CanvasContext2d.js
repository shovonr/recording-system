/* globals define */

define([], function () {
  "use strict";

  function CanvasContext2d(canvas) {
    var context = canvas.getContext("2d")
      , canvasWidth = canvas.width
      , canvasHeight = canvas.height
      , clearColor = "white"
      ;

    this.setClearColor = function (color) { clearColor = color; };
    this.getWidth = function () { return canvasWidth; };
    this.getHeight = function () { return canvasHeight; };

    this.fillRect = function (x, y, w, h, color) {
      context.fillStyle = color;
      context.fillRect(x, y, w, h);
    };

    this.onDraw = function (fn) {
      var self = this;
      window.requestAnimationFrame(function () {
        context.clearRect(0, 0, canvasWidth, canvasHeight);
        context.fillStyle = clearColor;
        context.fillRect(0, 0, canvasWidth, canvasHeight);
        fn();
        self.onDraw(fn);
      });
    };
  }

  return CanvasContext2d;
});