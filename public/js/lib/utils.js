/* globals define */

define(["exports", "underscore"], function (exports, _) {
  "use strict";

  exports.getUserMediaAsync = function (options, callback) {
    navigator.getUserMedia(options, function (stream) {
      callback(null, stream);
    }, function (err) {
      callback(err);
    });
  };

  _.assign(exports, _);
});